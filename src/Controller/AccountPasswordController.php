<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AccountPasswordController extends AbstractController
{
    /**
     * @Route("/account/update-password", name="app_account_password")
     */
    public function index(Request $request, UserPasswordHasherInterface $hasher, EntityManagerInterface $em): Response
    {
        //récupération du mail de l'utilisateur connecté
        $user = $this->getUser();
        $notification = null;
        $color = 'primary';

        //création du formulaire
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $old_pwd = $form->get('old_password')->getData();
            $new_pwd = $form->get('new_password')->getData();

            if($hasher->isPasswordValid($user, $old_pwd)) {
                $new_pwd = $form->get('new_password')->getData();
                $new_password = $hasher->hashPassword($user, $new_pwd);

                $user->setPassword($new_password);
                $em->flush();
                $notification = 'Votre mot de passe a bien été mis à jour';
                $color = 'info';
            }
            else {
                $notification = 'Votre mot de passe actuel n\'est pas le bon';
                $color = 'warning';
            }
        }

        return $this->render('account/update_password.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification,
            'color' => $color

        ]);
    }
}
