<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{

    /**
     * @Route("/inscription", name="register")
     */
    public function index(Request $request, UserPasswordHasherInterface $hasher, ManagerRegistry $manager): Response
    {
    $user = new User();
    $form = $this->createForm(RegisterType::class, $user);

    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid())
    {
        $em = $manager->getManager();
        $em->persist($user);

        $password = $user->getPassword();
        $hashedPassword = $hasher->hashPassword( $user, $password );

        $user->setPassword($hashedPassword);

        $em->flush();

        //return $this->render('home/password.html.twig', ["firstname" => "alex"]);
    }

    return $this->render('register/index.html.twig', ['form' => $form->createView(),]);
    }
}
